#include<stdio.h>
#include <errno.h>
#include <limits.h>
#include <stdlib.h>
#include <stdio.h>
#include <stdarg.h>
#include <string.h>
#include <netdb.h>
#include <arpa/inet.h>
#include <unistd.h>
#include <time.h>

#define BUFF_SIZE 1024

int displayPrompt();
void getData(int selection);
int connectToHost(char *hostname, char *port);
void recv_or_exit(int fd, char* buff, size_t max_len);
void send_or_exit(int fd, char* buff, size_t buff_len);


int main(){
    printf("\nWELCOME TO THE COMP375 SENSOR NETWORK\n\n\n");
    while(1){
        int selection = displayPrompt();
        if(selection == -1)
            continue;
        getData(selection);
    }   
    return 0;
}

/*
 * Display prompt. Take user input. Return user input
 */
int displayPrompt(){
   //Display prompt
    printf("Which sensor would you like to read:\n");
    printf("\t (1) Air temperature \n");
    printf("\t (2) Relative humidity \n");
    printf("\t (3) Wind speed \n");
    printf("\t (4) Quit Program \n");

    //Get user input
    char selection_string[1];
    fgets(selection_string, 4, stdin);     
    int selection = atoi(selection_string);
    
    if(selection != 1 && selection != 2 && selection != 3 && selection != 4){
        selection = 0;
    }
    else{        
        printf("Selection: %d \n", selection);
    }

    return selection;
}


/*
 * Connects with server to get data, branches on input selection, sends appropriate request to server, then prints received data.
 * @param selection: int indicating requested data (1-temp, 2-humidity, 3-wind speed, 4-qiot)
 */
void getData(int selection){
    //Declare variables
    int fd = 0;
    char* message = '\0'; 
    time_t epoch_time = NULL;
    struct tm* reported_time = localtime(&epoch_time);
    char* date = '\0';
    char intermediate_port[6];
    memset(intermediate_port, 0, 5);

    //Create empty buffer to store messages
    char buff[BUFF_SIZE];
    memset(buff, 0, BUFF_SIZE);

    //Connect to comp375.sandiego.edu 
    fd = connectToHost("comp375.sandiego.edu", "47789");  
    message = "AUTH password123\n";
    send_or_exit(fd, message, strlen(message));
    memset(buff, 0, BUFF_SIZE);
    recv_or_exit(fd, buff, BUFF_SIZE); 
    close(fd);
 
    //Extract dynamic port number for next connection       
    int i = 0; 
    for(i = 0; i < 5; i++){
        intermediate_port[i] = buff[i+28];
    }
    intermediate_port[5] = '\0';

    //Connect to sensor.sandiego.edu
    memset(buff, 0, BUFF_SIZE);
    fd = connectToHost("sensor.sandiego.edu", intermediate_port); 
    message = "AUTH sensorpass321\n";
    send_or_exit(fd, message, strlen(message));
    memset(buff, 0, BUFF_SIZE);
    recv_or_exit(fd, buff, BUFF_SIZE); 
    memset(buff, 0, BUFF_SIZE);
    
    switch(selection){
        case 1: 
            //GET AIR TEMP
            message = "AIR TEMPERATURE\n";
            send_or_exit(fd, message, strlen(message));
            memset(buff, 0, BUFF_SIZE);
            recv_or_exit(fd, buff, BUFF_SIZE); 
            
            //Grab temperature out of buffer
            char temperature[2];            
            temperature[0] = buff[11];
            temperature[1] = buff[12];
            temperature[2] = '\0';
            
            //Get time        
            time(&epoch_time);
            reported_time = localtime(&epoch_time);
            date = asctime(reported_time); 
            
            //Display result
            printf("\nThe last AIR TEMPERATURE reading was %s F, taken at %s \n", temperature, date);
            
            //Close connection to sensor.sandiego
            close(fd); 
            break;
        case 2:
            //GET HUMIDITY
            message = "RELATIVE HUMIDITY\n";
            send_or_exit(fd, message, strlen(message));
            memset(buff, 0, BUFF_SIZE);
            recv_or_exit(fd, buff, BUFF_SIZE); 
        
            //Grab humidity reading out of buffer
            char humidity[2];
            humidity[0] = buff[11];
            humidity[1] = buff[12];
            humidity[2] = '\0';
           
            //Get time         
            time(&epoch_time);
            reported_time = localtime(&epoch_time);
            date = asctime(reported_time);
                        
            //Display result
            printf("\nThe last RELATIVE HUMIDITY reading was %s percent, taken at %s \n", humidity, date);
           
            //Close connection to sensor.sandiego.edu
            close(fd);
            break;
        case 3:
            //GET WINDSPEED
            message = "WIND SPEED\n";
            send_or_exit(fd, message, strlen(message));
            memset(buff, 0, BUFF_SIZE);
            recv_or_exit(fd, buff, BUFF_SIZE); 

            //Grab wind 
            char wind[2];
            wind[0] = buff[11];
            wind[1] = buff[12];
            wind[2] = '\0';
                    
            //Get time
            time(&epoch_time);
            reported_time = localtime(&epoch_time);
            date = asctime(reported_time); 

            //Display result
            printf("\nThe last WIND SPEED reading was %s MPH, taken at %s \n", wind, date);

            //Close connection to sensor.sandiego.edu 
            close(fd);
            break;
        case 4:
            printf("GOODBYE! \n");
            exit(0);
            break;
        case 0:
            printf("***Invalid selection \n");
            break;
        default:
            printf("***Invalid selection \n");
            break;
    }
}

/*
 * Connects to host specified by hostname 
 * Sets up a socket and connects to it
 * Returns a file descriptor of the socket
 */
int connectToHost(char *hostname, char *port){
    //Fill in address info to prepare socket setup
    int status;
    struct addrinfo hints;
    struct addrinfo* servinfo;

    memset(&hints, 0, sizeof hints);
    hints.ai_family = AF_INET;
    hints.ai_socktype = SOCK_STREAM;

    //Prepare to connect
    status = getaddrinfo(hostname, port, &hints, &servinfo);
    if(status != 0){
        fprintf(stderr, "getaddrinfo error %s \n", gai_strerror(status));
        exit(1);
    }

    //Creates an endpoint for communication and returns a descriptor
    int fd = socket(servinfo->ai_family, servinfo->ai_socktype, servinfo->ai_protocol);
    if (fd == -1){
        perror("socket");
        exit(1);
    }

    //Connect
    int connect_status = connect(fd, servinfo->ai_addr, servinfo->ai_addrlen);
    if(connect_status != 0){
        perror("connect");
        exit(1);
    }
    
    freeaddrinfo(servinfo);
    return fd;
}
/*
 * Transmits the message in buff to the socket described by fd. Exits on error.
 *
 * @param: fd: the socket to send the message to
 * @param buff: the message to send
 * @param buff_len: the length of the buffer containing the message
 */
void send_or_exit(int fd, char* buff, size_t buff_len){
    int sent = send(fd, buff, buff_len, 0);
    if(sent == 0){
        printf("Server connection closed unexpectedly. Good bye");
        exit(1);
    }
    else if(sent == -1){
        perror("send");
        exit(1);
    }
}

/*
 * Responsible for receiving a message
 *
 * @param fd: file descriptor of socket to listen on
 * @param buff: source address of the message to be filled in with received content
 * @param max_len: length of buffer
 * @return: none. buff has been filled with the bytes received on fd
 */
void recv_or_exit(int fd, char* buff, size_t max_len){
    int recvd = recv(fd, buff, max_len, 0);
    if(recvd == 0){
        printf("Server connection closed unexpectedly. Good bye. \n");
        exit(1);
    }

    else if(recvd == -1){
        perror("recv");
        exit(1);
    }
}


    

